The proliferation of Big Data applications has led to many different
ways of describing them.  Building upon a careful analysis of 51 NIST
use cases, earlier effort at understanding & characterizing
distributed applications and the history of benchmarks for parallel
high-performance application we introduce the concept of Big Data
Ogres as a way of discussing big data applications in a precise,
consistent as well as possible a quantitative fashion.

Ogres are a conceptual aide introduced as a way to improve our ability
to (A) characterize/understand Big data applications (B) classify big
data applications and (C) provide "data intensive" benchmarks that
will eventually provide a way to improve our ability to understand the
performance and functionality of a big data application.  Whereas
objectives A, B and C are somewhat related they will be treated
separately.

To this end we discuss the primary "features" of the 51 NIST use
cases; by highlight these features which are noteworthy properties,
and looking for recurring features across applications, an
appreciation of the important properties of big data applications
emerges.  Although features inform "ogres" they are not directly
related to the types of ogres.

However, the most frequently occuring "features" inform the
*properties* of the Ogres which we refer to as the "facets" of the
Ogres. We identify three principal facets: (i) Problem Architecture
and Execution facet, (ii) Computational Characteristics facet and
(iii) Data Source and Style facet. The first facet will influence the
runtime requirements and execution environments of the Ogres. The
latter two will influence the 'static' properties i.e., programming
models, algorithms and library interfaces needed so as to provide
access to the runtime and execution environment.

Last but not least, Ogres can be used to motivate "data intensive"
benchmarks. Given the many "features" of big data applications and the
many different ways to "deploy" them, it is not possible to provide a
single all-encompassing benchmark. However, Ogres guide the
development of a suite of benchmarks that have the potential to
provide quantitative insight into computational characteristics
facets.

